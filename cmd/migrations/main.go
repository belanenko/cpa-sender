package main

import (
	"flag"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.midas.dev/cpa-sender/migrations"
)

func main() {
	var mup, mdown bool
	flag.BoolVar(&mup, "mup", false, "up migrations")
	flag.BoolVar(&mdown, "mdown", false, "down migrations")
	flag.Parse()

	if mdown && mup {
		logrus.WithFields(
			logrus.Fields{
				"mup":   mup,
				"mdown": mdown,
			},
		).Info("migration not started")
		return
	}

	db, err := sqlx.Open("postgres", os.Getenv("PG_URL"))
	if err != nil {
		logrus.WithError(err).Fatal("sqlx open")
	}

	m := migrations.New(db)

	if mdown {
		if err := m.Down(); err != nil {
			logrus.WithError(err).Fatal("migrations down")
		}
	}

	if mup {
		if err := m.Up(); err != nil {
			logrus.WithError(err).Fatal("migrations up")
		}
	}
}
