package main

import (
	"github.com/caarlos0/env/v6"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.midas.dev/cpa-sender/configs"
	"gitlab.midas.dev/cpa-sender/internal/app/queue/pgqueue"
	"gitlab.midas.dev/cpa-sender/internal/repository"
	"gitlab.midas.dev/cpa-sender/internal/services"
)

func main() {
	cfg := configs.Config{}
	err := env.Parse(&cfg)
	if err != nil {
		logrus.WithError(err).Fatal("env parse")
	}

	err = cfg.Validate()
	if err != nil {
		logrus.WithError(err).Fatal("cfg validate")
	}

	db, err := sqlx.Connect("postgres", cfg.ConnectionURL)
	if err != nil {
		logrus.WithError(err).Fatal("db connect")
	}

	pgrep := pgqueue.New(db)
	repo := repository.NewRepository(pgrep)
	serv := services.NewService(repo)
	_ = serv
}
