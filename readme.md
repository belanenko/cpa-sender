# CPA sender

## Task description (on Russian)

Во внешней базе данных находятся три типа эвентов - регистрация, kyc-подтверждение, депозит $100+.

Необходимо получать эвенты из внешней базы и отправлять во внешний сервис.  
Для отправки эвента во внешний сервис необходимо вызвать API-метод внешнего сервиса.  
Пример метода апи внешнего сервиса: `https://example.com/track/goal-by-click-id?click_id=<CLICK_ID>&goal_alias=<EVENT_NAME>&conv_status=<STATUS>&token=<TOKEN>`

Каждый эвент должен быть отправлен только один раз.

## Requirements

1. `make` - GNU make utility to maintain groups of programs  

2. `.env` [optional `.env.local`] file with contains variables:  

> variables from `.env` will be shaded by variables from file `.env.local`
>
> ```bash
> PG_URL=postgres://...
> ```

---

## How to

### Make migrations

1. for up migrations use `make up`  
2. for down migrations use `make down`  
