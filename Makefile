include .env
-include .env.local
export

.DEFAULT_GOAL: run-sender

.PHONY: run-sender
run-sender: 
	go run cmd/sender/main.go

.PHONY: up
up: 
	go run cmd/migrations/main.go -mup=true

.PHONY: down
down: 
	go run cmd/migrations/main.go -mdown=true

