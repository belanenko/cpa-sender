package services

import (
	"time"

	"gitlab.midas.dev/cpa-sender/internal/domain"
	"gitlab.midas.dev/cpa-sender/internal/repository"
)

type Queue interface {
	AddEvents(events ...*domain.Event) error
	GetEventWithLock(locktime time.Duration) (*domain.Event, error)
	Approve(event *domain.Event) error
}

type service struct {
	Queue
}

func NewService(repo *repository.Repository) *service {
	return &service{
		Queue: NewQueueService(repo.QueueRepository),
	}
}
