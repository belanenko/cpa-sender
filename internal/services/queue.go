package services

import (
	"time"

	"gitlab.midas.dev/cpa-sender/internal/domain"
	"gitlab.midas.dev/cpa-sender/internal/repository"
)

type queueService struct {
	repo repository.QueueRepository
}

func NewQueueService(repo repository.QueueRepository) *queueService {
	return &queueService{
		repo: repo,
	}
}

func (s *queueService) AddEvents(events ...*domain.Event) error {
	return s.repo.AddEvents(events...)
}
func (s *queueService) GetEventWithLock(locktime time.Duration) (*domain.Event, error) {
	return s.repo.GetEventWithLock(locktime)
}
func (s *queueService) Approve(event *domain.Event) error {
	return s.repo.Approve(event)
}
