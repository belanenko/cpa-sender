package repository

import (
	"time"

	"gitlab.midas.dev/cpa-sender/internal/domain"
)

type EventSourceRepository interface {
	GetEvents() ([]*domain.Event, error)
}

type QueueRepository interface {
	AddEvents(events ...*domain.Event) error
	GetEventWithLock(locktime time.Duration) (*domain.Event, error)
	Approve(event *domain.Event) error
}

type Repository struct {
	QueueRepository
}

func NewRepository(queue QueueRepository) *Repository {
	return &Repository{
		QueueRepository: queue,
	}
}
