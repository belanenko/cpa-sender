package mock

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.midas.dev/cpa-sender/internal/domain"
)

func TestMock_AddEvents(t *testing.T) {
	mock := New()
	err := mock.AddEvents(&domain.Event{})
	assert.NoError(t, err)
	require.Len(t, mock.storage, 1)
}

func TestMock_GetEventWithLock(t *testing.T) {
	mock := New()
	testEvent := &domain.Event{}
	err := mock.AddEvents(testEvent)
	require.NoError(t, err)
	require.Len(t, mock.storage, 1)

	locktime := time.Second
	_, err = mock.GetEventWithLock(locktime)
	assert.NoError(t, err)

	e, err := mock.GetEventWithLock(locktime)
	assert.NoError(t, err)
	assert.Nil(t, e)

	time.Sleep(locktime)

	e, err = mock.GetEventWithLock(locktime)
	assert.NoError(t, err)
	assert.Equal(t, testEvent, e)
}

func TestMock_Approve(t *testing.T) {
	mock := New()

	testEvent := &domain.Event{}

	err := mock.AddEvents(testEvent)
	require.NoError(t, err)
	require.Len(t, mock.storage, 1)

	err = mock.Approve(testEvent)
	require.NoError(t, err)
	require.Len(t, mock.storage, 0)
}
