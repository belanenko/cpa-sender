package mock

import (
	"errors"
	"sync"
	"time"

	"gitlab.midas.dev/cpa-sender/internal/domain"
)

type event struct {
	Locked   bool
	LockedAt time.Time
	*domain.Event
}

type mock struct {
	sync.Mutex
	storage map[*event]struct{}
}

func New() *mock {
	return &mock{
		storage: make(map[*event]struct{}),
	}
}

func (m *mock) AddEvents(events ...*domain.Event) error {
	m.Lock()
	defer m.Unlock()

	for _, e := range events {
		m.storage[&event{Event: e}] = struct{}{}
	}

	return nil
}

func (m *mock) GetEventWithLock(locktime time.Duration) (*domain.Event, error) {
	m.Lock()
	defer m.Unlock()

	for k := range m.storage {
		if k.LockedAt.UTC().Unix() > time.Now().UTC().Unix() {
			continue
		}
		k.Locked = true
		k.LockedAt = time.Now().Add(locktime)
		return k.Event, nil
	}
	return nil, nil
}

func (m *mock) Approve(event *domain.Event) error {
	m.Lock()
	defer m.Unlock()

	for k := range m.storage {
		if k.Event == event {
			delete(m.storage, k)
			return nil
		}
	}

	return errors.New("event not found")
}
