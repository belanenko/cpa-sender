package pgqueue

import (
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.midas.dev/cpa-sender/internal/domain"
)

type itemStatus string

var (
	ErrNotImpl     = errors.New("not implemented")
	queueTableName = errors.New("cpa.queue")

	undefined itemStatus = "undefined"
	inwork    itemStatus = "inwork"
	done      itemStatus = "done"
)

type Storage struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Storage {
	return &Storage{
		db: db,
	}
}

func (s *Storage) AddEvents(events ...*domain.Event) error {
	// tx, err := s.db.Begin()
	// if err != nil {
	// 	return err
	// }

	// query := fmt.Sprintf(``)

	// tx.Exec()

	// return
	return ErrNotImpl
}
func (s *Storage) GetEventWithLock(locktime time.Duration) (*domain.Event, error) {
	// s.db.

	return nil, ErrNotImpl
}
func (s *Storage) Approve(event *domain.Event) error {
	// s.db.

	return ErrNotImpl
}
