package pgqueue

import "github.com/jmoiron/sqlx"

type Config struct {
	ConnectionURL string `env:"PG_URL"`
}

func (c *Config) Validate() error {
	db, err := sqlx.Connect("postgres", c.ConnectionURL)
	if err != nil {
		return err
	}
	defer db.Close()

	err = db.Ping()
	return err
}
