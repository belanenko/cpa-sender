package domain

import "errors"

type EventStatus string

type EventName string

const (
	Undefined EventStatus = "undefined"
	InWork    EventStatus = "inwork"
	Done      EventStatus = "done"

	Registration EventName = "registration"
	KYC          EventName = "kyc"
	Deposit      EventName = "deposit"
)

// https://capitalcpa.scaletrk.com/track/goal-by-click-id?click_id={CLICKID}&goal_alias=reg&conv_status=pending&token={TOKEN}
type Event struct {
	Click
	Goal
	Conv
	Creditionals
	Name   EventName
	Status EventStatus
}

type Creditionals struct {
	Token string `json:"token"`
}

type Goal struct {
	GoalID    string `json:"goal_id"`
	GoalAlias string `json:"goal_alias"`
}

type Conv struct {
	ConvStatus string `json:"conv_status"`
}

type Click struct {
	ClickID string `json:"click_id"`
}

func (e *Event) Validation() error {
	if e.Click.ClickID == "" {
		return errors.New("empty click id")
	}

	if e.Goal.GoalAlias == "" {
		return errors.New("empty goal alisas")
	}

	if e.Goal.GoalID == "" {
		return errors.New("empty goal id")
	}

	if e.Conv.ConvStatus == "" {
		return errors.New("empty conv status")
	}

	if e.Creditionals.Token == "" {
		return errors.New("empty creds")
	}

	return nil
}
