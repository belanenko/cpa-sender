package configs

import "gitlab.midas.dev/cpa-sender/internal/app/queue/pgqueue"

type Config struct {
	pgqueue.Config
}

func (c *Config) Validate() error {
	err := c.Config.Validate()
	if err != nil {
		return err
	}

	return nil
}
