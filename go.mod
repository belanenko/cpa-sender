module gitlab.midas.dev/cpa-sender

go 1.19

require (
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.6
	github.com/pressly/goose/v3 v3.7.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/caarlos0/env/v6 v6.10.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.10 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
