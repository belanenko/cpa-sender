-- +goose Up
-- +goose StatementBegin
CREATE TYPE queue_item_status AS ENUM ('undefined', 'inwork', 'done');

CREATE TABLE cpa.queue (
    id SERIAL PRIMARY KEY,
    event_name TEXT NOT NULL,
    event_id BIGINT NOT NULL,
    payload JSONB NOT NULL,
    status queue_item_status DEFAULT 'undefined',
    UNIQUE (table_name, row_id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE cpa.queue;
DROP TYPE queue_item_status;
-- +goose StatementEnd
