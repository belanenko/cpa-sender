package migrations

import (
	"embed"

	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose/v3"
)

//go:embed sql/*.sql
var sql embed.FS

type migrations struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *migrations {
	return &migrations{
		db: db,
	}
}

func (m *migrations) Up() error {
	goose.SetBaseFS(sql)
	if err := goose.SetDialect("postgres"); err != nil {
		return err
	}
	if err := goose.Up(m.db.DB, "sql"); err != nil {
		return err
	}
	return nil
}

func (m *migrations) Down() error {
	goose.SetBaseFS(sql)
	if err := goose.SetDialect("postgres"); err != nil {
		return err
	}
	if err := goose.DownTo(m.db.DB, "sql", -1); err != nil {
		return err
	}
	return nil
}
